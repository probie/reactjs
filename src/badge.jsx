var React = require('react');

//Define a react component class where the title and number are acquired from the options object
var badge = React.createClass({
    render: function() {
        return <button className="btn btn-primary" type="button">
            {this.props.title}
            <span className="badge">
                {this.props.classCount}
            </span>
        </button>
    }
});

module.exports = badge;
