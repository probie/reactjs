var React = require('react');
var ReactDOM = require('react-dom');
var CardList = require('./cardlist');

var options = {
    thumbnailData: [{
        title: 'Classes',
        classCount: 127,
        thumbTitle: 'LMS Online',
        number: 166,
        image: "http://placehold.it/350x150",
        imageAlt: "LMS Online Class List",
        description: "LMS Online is your one-stop shop for learning at YOUR pace. through our online curriculum you'll be streamlining your processes in no time! Contact one of our sales reps to start today!"
    },{
        title: 'Classes',
        classCount: 213,
        thumbTitle: 'LMS External',
        number: 169,
        image: "http://placehold.it/350x150",
        imageAlt: "LMS External Class List",
        description: "Expand your knowledge of our Learning Management System through our online courses. These courses are designed to get you up and running as quickly as possible."
    },{
        title: 'Classes',
        classCount: 86,
        thumbTitle: 'LMS Internal',
        number: 169,
        image: "http://placehold.it/350x150",
        imageAlt: "LMS External Class List",
        description: "Internal classes help your knowledge of our Learning Management System through our online learning courses. These courses are designed to get you up and running as quickly as possible."
    },{
        title: 'Classes',
        classCount: 12,
        thumbTitle: 'LMS Handbook',
        number: 169,
        image: "http://placehold.it/350x150",
        imageAlt: "LMS External Class List",
        description: "Internal classes help your knowledge of our Learning Management System through our online learning courses. These courses are designed to get you up and running as quickly as possible."
    }]
};

//instaniate our class here to include our badge component
//and make the options object available.
var element = React.createElement(CardList, options);

//When we ask react to render the class, we will tell it
//where to place the rendered element in the dom
React.render(element, document.querySelector('.target'));
