var React = require('react');
var Badge = require('./badge');

// JSX nested class
var Card = React.createClass({
    render: function() {
        return <div className="col-sm-3 thumbnail">
            <img src={this.props.image} alt={this.props.imageAlt} />
            <div className="caption">
                <h3>{this.props.thumbTitle}</h3>
                <p>{this.props.description}</p>
                <p>
                <Badge title={this.props.title} classCount={this.props.classCount} />
                </p>
            </div>
        </div>
    }
});

module.exports = Card;
