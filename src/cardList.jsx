var React = require('react');
var Card = require('./card');

var CardList = React.createClass({
    render: function() {
        if(this.props.thumbnailData) {
            var list = this.props.thumbnailData.map(function(cardProps){
                return <Card key={cardProps.id} {...cardProps} />
            });

            return <div>
                {list}
            </div>
        } else {
            return <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                No Thumbnail Data found.
            </div>
        }
    }
});

module.exports = CardList;
